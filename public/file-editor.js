var editor = ace.edit("editor");
editor.setTheme("ace/theme/monokai");
function setMode(fileName) {
    let modelist = ace.require("ace/ext/modelist");
    let mode = modelist.getModeForPath(fileName).mode;
    console.log('mode', mode);
    editor.session.setMode(mode);
}

var filePath = getParameterByName('path'),
    fileName = filePath.split('/').pop();
    console.log(filePath);

$("#endingText").val(filePath);

// filename with extension passed to ace setMode method
setMode(fileName);

$.get('/file?path=' + filePath, function (data, status) {
    editor.setValue(data, 1) // moves cursor to the end
})

function getParameterByName(name) {
    url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function saveData() {
    $.post("/updateFile?path=" + filePath, { data: editor.getValue() }, function (data, status) {
        if(status == 'success'){
            toastr.success('Successfully Edited');
        }
    });
}

