// entry point
const root = '/';
Dropzone.autoDiscover = false;

// jquery function which runs after dom loaded
$(function () {
    // whole body cached for performance

    function b64toBlob(b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;

        var byteCharacters = atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);
            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

        var blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }

    $body = $('body');
    $pathInput = $body.find('#path');
    $loader = $body.find('#loader');
    $pathInput.val(root);
    let zipTypeArray = ['application/x-rar-compressed', 'application/zip', 'application/tar']
    // path change detection for home button enabling
    $pathInput.change(function (e) {
        if ($pathInput.val() === root) {
            $body.find('#home').addClass('disabled');
            $body.find('#upOneLevel').addClass('disabled');
        } else {
            $body.find('#home').removeClass('disabled');
            $body.find('#upOneLevel').removeClass('disabled');
        }
    });
    // Initiate datatable
    let table = $body.find('#filesOrFolders').DataTable({
        'order': [[2, 'desc']]
    });
    // globally used data
    let oldPath, selectedRow, selectedRows, previousControls, currentControls, overwrite, skipTrash,
        renameElement, renameValue, renameIndex, renameCellPosition, firstClicked, previousValue, compressPath = false;
    let multipleData = [], historyList = [], forwardHistoryList = [];
    let jstreeClicked = 0;
    // initiate dropzone
    const options = {
        url: '/upload',
        parallelUploads: 100,
        uploadMultiple: true,
        init: function () {
            this.on('sendingmultiple', function (files, xhr, data) {
                data.append('path', $pathInput.val());
                data.append('overwrite', overwrite);
            });
            this.on('successmultiple', function (files) {
                reloadTable($pathInput.val())
                toastr.success('Successfully uploaded');
            });
            this.on('errormultiple', function (files, err, xhr) {
                toastr.error(err.error);
            });
        }
    };
    $('div#uploadBox').dropzone(options);

    // jstree div found with find method and core function added for listing folders in left side
    $body.find('#filesTree').jstree({
        'core': {
            'check_callback': true,
            'data': {
                'url': function (node) {
                    let url = '/listFolders?path=' + root;
                    if (node.id !== '#')
                        for (let i = node.parents.length - 2; i >= 0; i--)
                            url = url.concat('/' + node.parents[i].split('/').pop());
                    console.log('url:', url, node);
                    return url;
                },
                'data': function (node) {
                    if (node.id === '#')
                        getFilesAndFolders(root, this, node);
                    return { id: node.id };
                }
            }
        }
    });

    // click function for listing files and folders on right side and listing folders under new selected folder
    $body.on('click.jstree', '#filesTree', function (e) {
        console.log('jstree click is  ', this);
        let instance = $.jstree.reference(this),
            node = instance.get_node(e.target),
            filePath = root + (root !== '/' ? '/' : '') + instance.get_path(node, '/');
        $pathInput.val(filePath).trigger('change');
        jstreeClicked = 1;
        if ($pathInput.val() != root) {
            if (historyList.length == 0) {
                $body.find('#back').addClass('enabled');
            }
            historyList.push({ filePath, element: this });
            console.log('historyList is ', historyList);
        }
        getFilesAndFolders(filePath, instance, node);
    });

    // files and folders displayed on go button click or enter press on path input and node opened in jstree
    $body.on('click', '#goBtn', function () {
        let instance = $.jstree.reference($body.find('#filesTree')),
            filePath = $pathInput.val(),
            id = root !== filePath ? filePath.replace(root, root !== '/' ? '#' : '#/') : '#',
            node = instance.get_node(id);
        getFilesAndFolders(filePath, instance, node);
    })

    $body.on('keyup', '#path', function (e) {
        if (e.keyCode === 13) {
            let instance = $.jstree.reference($body.find('#filesTree')),
                filePath = $pathInput.val(),
                id = root !== filePath ? filePath.replace(root, root !== '/' ? '#' : '#/') : '#',
                node = instance.get_node(id);
            getFilesAndFolders(filePath, instance, node);
        }
    });

    // file upload modal
    $fileUploadModal = $body.find('#fileUploadModal');
    $overwriteCheckbox = $fileUploadModal.find('#overwrite');
    // overwrite checkbox change event
    $overwriteCheckbox.change(function () {
        overwrite = this.checked;
    });
    $fileUploadModal.on('hidden.bs.modal', function () {
        Dropzone.forElement('#uploadBox').removeAllFiles();
        $overwriteCheckbox.prop('checked', false).change();
    });

    // new folder modal
    $newFolderModal = $body.find('#newFolderModal');
    $newFolderName = $newFolderModal.find('#newFolderName');
    $newFolderPath = $newFolderModal.find('#newFolderPath');
    $newFolderModal.on('show.bs.modal', function () {
        $newFolderPath.val($pathInput.val());
    });
    $newFolderModal.on('hidden.bs.modal', function () {
        $newFolderModal.find('#folderNameError').remove();
        $newFolderName.val('');
        $newFolderPath.val('');
    });
    $newFolderModal.on('click', '#newFolderBtn', function () {
        $newFolderModal.find('#folderNameError').remove();
        let name = $newFolderName.val(),
            path = $newFolderPath.val();
        if (!!name)
            $.post('/folder?path=' + path + '&name=' + name)
                .done(function () {
                    let parentId = path.replace(root, root !== '/' ? '#' : '#/');
                    $('#filesTree').jstree().create_node(parentId, { 'id': parentId + '/' + name, 'text': name, icon: 'fas fa-folder' }, 'last', function () {
                        reloadTable(path);
                    });
                    toastr.success('Folder created');
                    $newFolderModal.modal('hide');
                })
                .fail(function () {
                    toastr.error('Folder already exists.');
                })
        else $newFolderName.after('<small id="folderNameError" class="text-danger">*This field is required<br></small>');
    });

    // new file modal
    $newFileModal = $body.find('#newFileModal');
    $newFileName = $newFileModal.find('#newFileName');
    $newFilePath = $newFileModal.find('#newFilePath');
    $newFileModal.on('show.bs.modal', function () {
        $newFilePath.val($pathInput.val());
    });

    $newFileModal.on('hidden.bs.modal', function () {
        $newFileModal.find('#fileNameError').remove();
        $newFileName.val('');
        $newFilePath.val('');
    });

    $newFileModal.on('click', '#newFileBtn', function () {
        $newFileModal.find('#fileNameError').remove();
        let name = $newFileName.val(),
            path = $newFilePath.val();
        if (!!name)
            $.post('/file?path=' + path + '&name=' + name)
                .done(function () {
                    reloadTable(path);
                    toastr.success('File created');
                    $newFileModal.modal('hide');
                })
                .fail(function () {
                    toastr.error('File already exists.');
                })
        else $newFileName.after('<small id="fileNameError" class="text-danger">*This field is required<br></small>');
    });

    // copy modal
    $copyModal = $body.find('#copyModal');
    $copyCurrentPath = $copyModal.find('#copyCurrentPath');
    $copyDestinationPath = $copyModal.find('#copyDestinationPath');
    $body.on('click', '#copy', function () {
        if ($body.find(this).hasClass('enabled')) {
            $copyModal.modal('show');
        } else return false;
    });
    $copyModal.on('show.bs.modal', function () {
        $copyCurrentPath.text($pathInput.val());
    });
    $copyModal.on('hidden.bs.modal', function () {
        $copyModal.find('#copyDestinationPathEmptyError').remove();
        $copyModal.find('#copyDestinationPathSameError').remove();
        $copyCurrentPath.text('');
        $copyDestinationPath.val('');
    });
    $copyModal.on('click', '#copyBtn', function () {
        $copyModal.find('#copyDestinationPathEmptyError').remove();
        $copyModal.find('#copyDestinationPathSameError').remove();
        let src = $copyCurrentPath.text(),
            dest = $copyDestinationPath.val();
        if (!!dest) {
            if (src === dest) $copyDestinationPath.after('<small id="copyDestinationPathSameError" class="text-danger">*Source and Destination can\'t be same<br></small>');
            else {
                let data = table.row(selectedRow).data(),
                    name = data[5].split('/').pop(),
                    payload = { src, dest, name };
                $.post('/copy', payload)
                    .done(function () {
                        toastr.success('Copied Successfully');
                        $copyModal.modal('hide');
                    })
                    .fail(function (err) {
                        console.log('copy error', err)
                        toastr.error('Something went wrong.');
                    })
            }
        }
        else $copyDestinationPath.after('<small id="copyDestinationPathEmptyError" class="text-danger">*This field is required<br></small>');
    });

    // move modal
    $moveModal = $body.find('#moveModal');
    $moveCurrentPath = $moveModal.find('#moveCurrentPath');
    $moveDestinationPath = $moveModal.find('#moveDestinationPath');
    $body.on('click', '#move', function () {
        if ($body.find(this).hasClass('enabled')) {
            $moveModal.modal('show');
        } else return false;
    });
    $moveModal.on('show.bs.modal', function () {
        $moveCurrentPath.text($pathInput.val());
    });
    $moveModal.on('hidden.bs.modal', function () {
        $moveModal.find('#moveDestinationPathEmptyError').remove();
        $moveModal.find('#moveDestinationPathSameError').remove();
        $moveCurrentPath.text('');
        $moveDestinationPath.val('');
    });
    $moveModal.on('click', '#moveBtn', function () {
        $moveModal.find('#moveDestinationPathEmptyError').remove();
        $moveModal.find('#moveDestinationPathSameError').remove();
        let src = $moveCurrentPath.text(),
            dest = $moveDestinationPath.val();
        if (!!dest) {
            if (src === dest) $moveDestinationPath.after('<small id="moveDestinationPathSameError" class="text-danger">*Source and Destination can\'t be same<br></small>');
            else {
                let data = table.row(selectedRow).data(),
                    name = data[5].split('/').pop(),
                    payload = { src, dest, name };
                $.post('/move', payload)
                    .done(function () {
                        reloadTable(src);
                        toastr.success('Moved Successfully');
                        $moveModal.modal('hide');
                    })
                    .fail(function (err) {
                        console.log('move error', err)
                        toastr.error('Something went wrong.');
                    })
            }
        }
        else $moveDestinationPath.after('<small id="moveDestinationPathEmptyError" class="text-danger">*This field is required<br></small>');
    });

    // delete modal
    $deleteModal = $body.find('#deleteModal');
    $forceDeleteFolderModal = $body.find('#forceDeleteFolderModal');
    $deletePath = $deleteModal.find('#deletePath');
    $skipTrashCheckbox = $deleteModal.find('#skipTrash');
    $body.on('click', '#delete', function () {
        if ($body.find(this).hasClass('enabled')) {
            $deleteModal.modal('show');
        }
    });
    // skip trash checkbox change event
    $skipTrashCheckbox.change(function () {
        skipTrash = this.checked;
    });
    $deleteModal.on('show.bs.modal', function () {
        $deletePath.text($pathInput.val());
    });
    $deleteModal.on('hidden.bs.modal', function () {
        $deletePath.text('');
        $skipTrashCheckbox.prop('checked', false).change();
    });
    $deleteModal.on('click', '#confirmDeleteBtn', function (e) {
        let data = table.row(selectedRow).data(),
            id = data[5],
            type = data[3],
            path = id.replace(root !== '/' ? '#' : '#/', root);
        if (skipTrash) { // delete file or folder permanently when skip trash checked
            $.ajax({
                url: `/${type === 'directory' ? 'folder' : 'file'}?path=${path}`,
                type: 'DELETE'
            }).done(function () {
                afterDelete();
                $deleteModal.modal('hide');
            }).fail(function (err) {
                if (type === 'directory') {
                    console.log('directory error', err)
                    $deleteModal.modal('hide');
                    $forceDeleteFolderModal.modal('show');
                }
                else toastr.error('Something went wrong.');
            });
        }
        else {
            // send file to trash when skip trash not checked
            console.log('do not delete file permanent by calling delete file or folder api send it to trash');
            $.ajax({
                url: `/trash?path=${path}&root=${root}`,
                type: 'DELETE'
            }).done(function (file, status) {
                console.log('response is ', file, status);
                if (file.message == 'success') {
                    afterDelete();
                }
                else {
                    toastr.error('Something went wrong.');
                }
            }).fail(function (err) {
                console.log(err);
                toastr.error('Something went wrong.');
            });
        }
    });

    $forceDeleteFolderModal.on('click', '#confirmForceDeleteBtn', function (e) {
        let data = table.row(selectedRow).data(),
            id = data[5],
            path = id.replace(root !== '/' ? '#' : '#/', root);
        $.ajax({
            url: `/forceFolder?path=${path}`,
            type: 'DELETE'
        }).done(function () {
            afterDelete();
            $forceDeleteFolderModal.modal('hide');
        }).fail(function (err) {
            toastr.error('Something went wrong.');
        });
    });

    $body.on('click', '#viewTrash', function (e) {
        if (!$body.find(this).hasClass('disabled')) {
            $body.find('#emptyTrash').removeClass('disabled').addClass('enabled');
            $body.find('#viewTrash').addClass('disabled');
            let instance = $.jstree.reference($body.find('#filesTree')),
                id = root.replace(root, root !== '/' ? '#' : '#/'),
                node = instance.get_node(id);
            oldPath = '';
            //console.log('node value on view trash is ', node);
            //getFilesAndFolders('C:/$Recycle.Bin/', instance, node);
        } else return false;
    })

    $body.on('click', '#emptyTrash', function (e) {
        if (!$body.find(this).hasClass('disabled')) {
            $.ajax({
                url: '/emptyTrashBin',
                type: 'DELETE'
            }).done(function () {
                console.log('success');
            })
        } else return false;
    })

    $body.on('click', '#restore', function (e) {
        if ($body.find(this).hasClass('enabled')) {
            let data = table.row(selectedRow).data(),
                splitFileOrFolder = data[0].split('</i>')[1],
                postData = { path: '/test/trash/', name: splitFileOrFolder };
            console.log('selected row data on restore is ', postData);
            $.post('/restoreFileOrFolder', postData, function (file, status) {
                console.log('file and status is ', file, status);
            })
        }
    })

    $body.on('click', '#download', function () {
        if ($body.find(this).hasClass('enabled')) {
            let data = table.row(selectedRow).data(),
                id = data[5],
                path = id.replace(root !== '/' ? '#' : '#/', root);
            downloadFile(path)
        } else return false;
    });

    $RenameModal = $body.find('#RenameModal');
    $body.on('click', '#rename', function (e) {
        if ($body.find(this).hasClass('enabled')) {
            if (renameCellPosition, renameValue, renameIndex, renameElement) {
                //RenameFiles(renameCellPosition, renameValue, renameIndex, renameElement);
                let splitRenameValue = renameValue[0].split('</i>')[1];
                $RenameModal.find('#oldName').append('<p>' + splitRenameValue + '</p>');
                $RenameModal.find('#renameValue').val(renameValue[0].split('</i>')[1]);
                $RenameModal.modal('show');
            }
        } else return false;
    });

    $RenameModal.on('click', '#renameBtn', function (e) {
        let dataas = table.row(renameElement).data();
        let ids = dataas[5];
        let filePath = ids.replace(root !== '/' ? '#' : '#/', root)
        let fileRename = $RenameModal.find('#renameValue').val();
        $.ajax({
            url: `/renameFile?path=${filePath}&name=${fileRename}`,
            type: 'POST'
        }).done(function (datas, statusText, xhr) {
            if (datas.message == 'success') {
                let newValue = datas.newFile.replace('/' + fileRename, '');
                reloadTable(newValue);
                $body.find('#filesTree').jstree('refresh');
                $RenameModal.modal('hide');
                $RenameModal.find('#oldName p').remove();
                toastr.success('Successfully Updated');
            }
            else {
                toastr.error('File already exists');
            }
        }).fail(function () {
            toastr.error('Something went wrong.');
        });
    })

    $permissionsModal = $body.find('#permissionsModal');
    $body.on('click', '#permissions', function (e) {
        if ($body.find(this).hasClass('enabled')) {
            let datas = table.row('.selected').data(),
                newValue = datas[0].split('</i>')[1],
                permissionValue = datas[4],
                dataPath = datas[5];
            $permissionsModal.find('#permissionFileName').append('<p>' + newValue + '</p>');
            $permissionsModal.modal('show');
            console.log('permission value is ', permissionValue[1]);
            $permissionsModal.find('#firstDigitPermission').val(permissionValue[1]);
            $permissionsModal.find('#secondDigitPermission').val(permissionValue[2]);
            $permissionsModal.find('#thirdDigitPermission').val(permissionValue[3]);
            $permissionsModal.find('td input:checkbox').each(function (value, obje) {
                if (value == 0 || value == 3 || value == 6) {
                    let permValue = permissionValue.substr(1, 1);
                    if (permValue == 4 && value == 0) {
                        $permissionsModal.find(this).prop('checked', true);
                    }
                    else if (permValue == 3 && (value == 3 || value == 6)) {
                        $permissionsModal.find(this).prop('checked', true);
                    }
                    else if (permValue == 1 && value == 6) {
                        $permissionsModal.find(this).prop('checked', true);
                    }
                    else if (permValue == 2 && value == 3) {
                        $permissionsModal.find(this).prop('checked', true);
                    }
                    else if (permValue == 6 && (value == 0 || value == 3)) {
                        $permissionsModal.find(this).prop('checked', true);
                    }
                    else if (permValue == 5 && (value == 0 || value == 6)) {
                        $permissionsModal.find(this).prop('checked', true);
                    }
                    else if (permValue == 7 && (value == 0 || value == 3 || value == 6)) {
                        $permissionsModal.find(this).prop('checked', true);
                    }
                }
                else if (value == 1 || value == 4 || value == 7) {
                    let permValue = permissionValue.substr(2, 1);
                    if (permValue == 4 && value == 1) {
                        $permissionsModal.find(this).prop('checked', true);
                    }
                    else if (permValue == 6 && (value == 1 || value == 4)) {
                        $permissionsModal.find(this).prop('checked', true);
                    }
                    else if (permValue == 5 && (value == 1 || value == 7)) {
                        $permissionsModal.find(this).prop('checked', true);
                    }
                    else if (permValue == 7 && (value == 1 || value == 4 || value == 7)) {
                        $permissionsModal.find(this).prop('checked', true);
                    }
                    else if (permValue == 3 && (value == 4 || value == 7)) {
                        $permissionsModal.find(this).prop('checked', true);
                    }
                    else if (permValue == 2 && (value == 4)) {
                        $permissionsModal.find(this).prop('checked', true);
                    }
                    else if (permValue == 1 && (value == 7)) {
                        $permissionsModal.find(this).prop('checked', true);
                    }
                }
                else if (value == 2 || value == 5 || value == 8) {
                    let permValue = permissionValue.substr(3, 1);
                    if (permValue == 4 && value == 2) {
                        $permissionsModal.find(this).prop('checked', true);
                    }
                    else if (permValue == 6 && (value == 2 || value == 5)) {
                        $permissionsModal.find(this).prop('checked', true);
                    }
                    else if (permValue == 5 && (value == 2 || value == 8)) {
                        $permissionsModal.find(this).prop('checked', true);
                    }
                    else if (permValue == 7 && (value == 2 || value == 5 || value == 8)) {
                        $permissionsModal.find(this).prop('checked', true);
                    }
                    else if (permValue == 3 && (value == 5 || value == 8)) {
                        $permissionsModal.find(this).prop('checked', true);
                    }
                    else if (permValue == 2 && (value == 5)) {
                        $permissionsModal.find(this).prop('checked', true);
                    }
                    else if (permValue == 1 && (value == 8)) {
                        $permissionsModal.find(this).prop('checked', true);
                    }
                }
            });
        }
    })

    $permissionsModal.on('click', '#permissionCloseBtn', function (e) {
        $permissionsModal.find('#permissionFileName p').remove();
        $permissionsModal.find('td input:checkbox').each(function (value, obje) {
            $permissionsModal.find(this).prop('checked', false);
        })
    })

    $permissionsModal.on('change', 'input', function (e) {
        let DigitCounter = [];
        DigitCounter[0] = 0;
        DigitCounter[1] = 0;
        DigitCounter[2] = 0;
        $permissionsModal.find('td input:checkbox').each(function (value, obje) {
            if (obje.checked) {
                if (value == 0 || value == 3 || value == 6) {
                    if (value == 0) {
                        DigitCounter[0] += 4;
                    }
                    else if (value == 3) {
                        DigitCounter[0] += 2;
                    }
                    else if (value == 6) {
                        DigitCounter[0] += 1;
                    }
                }
                else if (value == 1 || value == 4 || value == 7) {
                    if (value == 1) {
                        DigitCounter[1] += 4;
                    }
                    else if (value == 4) {
                        DigitCounter[1] += 2;
                    }
                    else if (value == 7) {
                        DigitCounter[1] += 1;
                    }
                }
                else if (value == 2 || value == 5 || value == 8) {
                    if (value == 2) {
                        DigitCounter[2] += 4;
                    }
                    else if (value == 5) {
                        DigitCounter[2] += 2;
                    }
                    else if (value == 8) {
                        DigitCounter[2] += 1;
                    }
                }
            }
        });
        $permissionsModal.find('#firstDigitPermission').val(DigitCounter[0]);
        $permissionsModal.find('#secondDigitPermission').val(DigitCounter[1]);
        $permissionsModal.find('#thirdDigitPermission').val(DigitCounter[2]);
    })

    $permissionsModal.on('click', '#permissionBtn', function (e) {
        let datas = table.row('.selected').data(),
            filePath = datas[5],
            fullFilePath = filePath.replace(root !== '/' ? '#' : '#/', root),
            firstDigit = $permissionsModal.find('#firstDigitPermission').val(),
            secondDigit = $permissionsModal.find('#secondDigitPermission').val(),
            thirdDigit = $permissionsModal.find('#thirdDigitPermission').val(),
            fullDigit = firstDigit + '' + secondDigit + '' + thirdDigit;
        fullDigit = parseInt(fullDigit);
        let payload = { path: fullFilePath, mode: fullDigit }
        console.log('payload of permission is ', payload);
        $.post('/changePermission', payload)
            .done(function (status, file) {
                console.log('file and status is ', status, file);
                toastr.success('Changed Permission!!');
            })
            .fail(function () {
                console.log('failed')
            })
    })

    table.on('dblclick', 'tbody td', function (e) {
        e.preventDefault();
        let currentRow = $body.find(this).closest('tr'),
            data = table.row(currentRow).data(),
            id = data[5],
            filePath = id.replace(root !== '/' ? '#' : '#/', root);
        if ($body.find(this).index() !== 0 && $body.find(this).index() !== 4) {
            if (data[3] !== 'directory') {
                downloadFile(filePath);
            }
            else {
                $body.find('#back').addClass('enabled');
                let instance = $.jstree.reference($body.find('#filesTree')),
                    node = instance.get_node(id);
                $pathInput.val(filePath).trigger('change');
                getFilesAndFolders(filePath, instance, node);
            }
        }
        else {
          if (e.target.nodeName === 'SPAN') {
            if (firstClicked !== data[0] && firstClicked !== data[4]) {
              console.log('data on dblclick is ', firstClicked);
              RenameFiles(this, data, $body.find(this).index(), currentRow);
            }
          }
        }
    });

    $body.on('click', '#edit', function (e) {
        if ($body.find(this).hasClass('enabled')) {
            let data = table.row(selectedRow).data(),
                ids = data[5],
                path = ids.replace(root !== '/' ? '#' : '#/', root);
            console.info(data);
            console.log('selectedRow ', path);
            window.open(`file-editor.html?path=${path}`, '_blank');
        } else return false;
    });

    $body.on('click', '#view', function (e) {
        if ($body.find(this).hasClass('enabled')) {
            let data = table.row(selectedRow).data(),
                ids = data[5], 
                path = ids.replace(root !== '/' ? '#' : '#/', root);
            $.get(`/download?path=${path}`).done(function (file, status) {
                // console.log('file and status is ', file, status);
                // let filess = new Blob([file.data], { type: file.type });
                // let fileURL = URL.createObjectURL(filess);
                // let myWindow = window.open(filess, "myWindow", "width=300,height=300");
                // window.open('file:///‪E:/test/index.html');
  
              let base64URL = `data:${file.type};base64,${file.data}`;
              console.log('base64URL ', base64URL);
              /**
               * Display a base64 URL inside an iframe in another window.
               */
              let win = window.open();
              win.document.write('<iframe src="' + base64URL + '" frameborder="0" style="border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;" allowfullscreen></iframe>');
              win.document.title = file.name;
  
            })
            // let myWindow = window.open("", "myWindow", "width=300,height=300");
            // myWindow.document.write("<p>This is the source window!</p>");
        } else return false;
    });


    $body.on('click', '#reload', function (e) {
        reloadTable($pathInput.val());
    });

    $body.on('click', '#home', function (e) {
        if (!$body.find(this).hasClass('disabled')) {
            let tree = $('#filesTree').jstree(true);
            tree.refresh(false, function (state) {
                state.core.selected = [];
                return state;
            });
            $pathInput.val(root).trigger('change');
        } else return false;
    });

    $body.on('click', '#upOneLevel', function (e) {
        if (!$body.find(this).hasClass('disabled')) {
            let path = $pathInput.val();
            path = path.substr(0, path.lastIndexOf('/'));
            reloadTable(path);
            $pathInput.val(path).trigger('change');
        } else return false;
    });

    // select all and unselect all functionalities
    $selectAll = $body.find('#selectAll')
    $unselectAll = $body.find('#unselectAll')
    $body.on('click', '#selectAll', function (e) {
        if (!$body.find(this).hasClass('disabled')) {
            selectedRows = $body.find('tbody tr');
            $.each(selectedRows, function () {
                $body.find(this).addClass('selected');
            });
            toggleSelectUnselectControl('select');
        } else return false;
    });
    $body.on('click', '#unselectAll', function (e) {
        if (!$body.find(this).hasClass('disabled')) {
            $.each(selectedRows || selectedRow, function () {
                $body.find(this).removeClass('selected');
            });
            toggleSelectUnselectControl('unselect');
        } else return false;
    });

    $compressModal = $body.find('#compressModal');
    $compresResultModal = $body.find('#compressListModal');

    $compresResultModal.find('#closeListBtn').on('click', function (e) {
        $compresResultModal.find('li').remove();
    })

    $body.find("input[type='radio'][name='tarRadio']").on('click', function (e) {
        console.log('value is ', +$compressModal.find("input[type='radio'][name='tarRadio']:checked").text());
    })

    $body.on('click', '#compressConfirmBtn', function (e) {
        //let fileOrFolerName = renameValue[0].split('</i>')[1];
        console.log('multiple data value is ', multipleData);
        let pathValue = $pathInput.val();
        let path_Name = $compressModal.find('#compressDirInput').val();
        let split_Path_Name = path_Name.split('.')[0];
        let extName = $compressModal.find("input[type='radio'][name='tarRadio']:checked").val();
        split_Path_Name = split_Path_Name + '.' + extName;
        console.log('compres select value is ', split_Path_Name);
        let query = { path: pathValue, extName, newPath: split_Path_Name, totalFile: multipleData.length };
        multipleData.forEach(function (element, indexN) {
            query['selectedFile' + indexN] = element;
        })
        console.log('post query is ', query);
        $.post('/CreateZipFile', query, function (file, status) {
            console.log('file and status is ', file, status);
            if (file.message == 'success') {
                toastr.success('Zip created Successfully!');
                file.fullValue.forEach(function (element) {
                    $compresResultModal.find('ul')
                        .append('<li>adding: ' + element + '</li>')
                });
                $compressModal.modal('hide');
                $compresResultModal.modal('show');
                reloadTable(pathValue);
            }
            else {
                toastr.error('Error when creating zip!');
            }
        })
    })

    $body.on('click', '#compress', function (e) {
        if ($(this).hasClass('enabled')) {
            //console.log('total rows count is ', table.rows('.selected').data().length);
            let firstValue;
            table.rows('.selected').data()
                .each(function (value, index) {
                    let splitValue = value[0];
                    let newValue = splitValue.split('</i>')[1];
                    multipleData.push(newValue);
                    let fullPath = $pathInput.val() + '/' + newValue;
                    if (index == 0) {
                        firstValue = fullPath
                    }
                    $body.find('#listSection').append("<p>" + fullPath + "</p>");
                })
            let extension_value = $compressModal.find("input[type='radio'][name='tarRadio']:checked").val();
            firstValue = firstValue.split('.')[0];
            firstValue = firstValue + '.' + extension_value;
            $compressModal.find('#compressDirInput').val(firstValue);
            $compressModal.modal('show');
        }
    })

    $extractModal = $body.find('#extractModal');
    $body.on('click', '#extract', function (e) {
        if (!$body.find(this).hasClass('disabled')) {
            $extractModal.modal('show');
            let pathValue = $pathInput.val();
            let fileOrFolerName = renameValue[0].split('</i>')[1];
            $extractModal.find('.extractContainer').append(`<p>${pathValue + '/' + fileOrFolerName}</p>`);
            $extractModal.find('#extractDirInput').val(pathValue);
        }
    })

    $extractModal.on('click', '#extractCancel', function (e) {
        $extractModal.find('.extractContainer p').remove();
        $extractModal.modal('hide');
    })

    $extractResultModal = $body.find('#extractResultModal');
    $extractModal.on('click', '#extractConfirmBtn', function (e) {
        console.log('this is the click event');
        let pathValue = $pathInput.val();
        let newpathValue = $extractModal.find('#extractDirInput').val();
        let fileOrFolerName = renameValue[0].split('</i>')[1];
        let query = { path: pathValue, selectedFile: fileOrFolerName, newpathValue };
        $.post('/unzipFileOrFolder', query, function (file, status) {
            console.log('file and status is ', file, status);
            console.log('file and status of unzip is ', file, status);
            $extractResultModalP = $extractResultModal.find('#extractListSection');
            if (file.message == 'success') {
                file.fullValue.forEach(function (element) {
                    $extractResultModalP.append(`<p>Extracting: ${element}</p>`);
                })
                $extractModal.modal('hide');
                $extractResultModal.modal('show');
                toastr.success('Extract Successfull!');
                reloadTable(pathValue);
            }
            else {
                toastr.error('Error when creating extracting!');
            }
        })
    })

    $extractResultModal.on('click', '#closeExtractResultBtn', function (e) {
        $extractResultModal.find('#extractListSection p').remove();
    })

    $body.on('click', '#back', function (e) {
        let instance = $.jstree.reference($body.find('#filesTree'));
        let id, node, rootNode = false;
        if (historyList.length > 1) {
            if (historyList.length > 1 && jstreeClicked == 1) {
                forwardHistoryList.push(historyList.pop());
            }
            let lastFile = historyList.pop();
            id = lastFile.filePath.replace(root, root !== '/' ? '#' : '#/'),
                node = instance.get_node(id);
            rootNode = lastFile.filePath;
            //forwardHistoryList.push(lastFile);
        }
        else {
            if (historyList.length == 1) {
                let lastFile = historyList.pop();
                id = lastFile.filePath.replace(root, root !== '/' ? '#' : '#/'),
                    node = instance.get_node(id);
                rootNode = lastFile.filePath;
                //forwardHistoryList.push(lastFile);
            }
            else {
                id = root.replace(root, root !== '/' ? '#' : '#/');
                node = instance.get_node(id);
                rootNode = root;
                $body.find(this).removeClass('enabled');
                jstreeClicked = 0;
            }
        }
        if (instance && node) {
            getFilesAndFolders(rootNode, instance, node);
            $pathInput.val(rootNode);
            $body.find('#forward').addClass('enabled');
            console.log('historyList is ', historyList);
        }
    })

    $body.on('click', '#forward', function (e) {
        if (!$body.find(this).hasClass('disabled')) {
            let lastFile = forwardHistoryList.pop(),
                instance = $.jstree.reference($body.find('#filesTree')),
                id = lastFile.filePath.replace(root, root !== '/' ? '#' : '#/'),
                node = instance.get_node(id);
            historyList.push(lastFile);
            getFilesAndFolders(lastFile.filePath, instance, node);
        }
    })

    // <----------Data Table Events-----------> 
    table.on('click', 'tbody tr', function (e) {
        e.preventDefault();
        let fullData;
        if (e.ctrlKey) {
            $(this).toggleClass('selected');
        }
        else {
            selectedRow = $body.find(this);
            let data = table.row(selectedRow).data();
            currentControls = data[3] === 'directory' ? ['.fileAndFolder'] : zipTypeArray.includes(data[3]) ? ['.file', '.fileAndFolder', '#extract'] : ['.file', '.fileAndFolder'];
            if (selectedRow.hasClass('selected')) {
                selectedRow.removeClass('selected');
                $unselectAll.addClass('disabled');
                disableCurrentControls();
            }
            else {
                table.$('tr.selected').removeClass('selected');
                selectedRow.addClass('selected');
                if ($unselectAll.hasClass('disabled')) $unselectAll.removeClass('disabled');
                disablePreviousControls();
                enableCurrentControls();
            }
        }
    });

    // page change event
    table.on('page.dt', function () {
        if (selectedRows || selectedRow) {
            $.each(selectedRows || selectedRow, function () {
                $(this).removeClass('selected');
            });
            toggleSelectUnselectControl('unselect');
        }
    })

    table.on('click', 'tbody td', function (e) {
        e.preventDefault();
        console.log('tbody td is clicked ');
        let currentRow = $body.find(this).closest('tr');
        renameElement = currentRow;
        renameValue = table.row(currentRow).data();
        renameIndex = $body.find(this).index();
        renameCellPosition = this;
        console.log('current row is ', renameElement)
    })

    table.on('keyup', 'tbody td input', function (e) {
        let dataas = table.row(renameElement).data();
        let ids = dataas[5];
        let filePath = ids.replace(root !== '/' ? '#' : '#/', root)
        let beforeFileValue = dataas[0].split('</i>');
        if (e.key === 'Enter') {
            console.log('input value is ', renameElement);
            let fileRename = $body.find(this).val();
            // console.log('id is', ids);
            // console.log('fileRename is ', filePath, fileRename);
            $.ajax({
                url: `/renameFile?path=${filePath}&name=${fileRename}`,
                type: 'POST'
            }).done(function (datas, statusText, xhr) {
                if (datas.message == 'success') {
                    let newValue = datas.newFile.replace('/' + fileRename, '');
                    reloadTable(newValue);
                    $body.find('#filesTree').jstree('refresh');
                    toastr.success('Successfully Updated');
                }
                else {
                    if(!fileRename) {
                      toastr.warning('Enter name first !');
                      return;
                    }
                    toastr.error('File already exists');
                }
            }).fail(function () {
                toastr.error('Something went wrong.');
            });
        }
        else if (e.key === 'Escape') {
            //alert('escape key is pressed!!')
            console.log('value is escape ', renameIndex);
            if (renameIndex == 0) {
                dataas[0] = `${beforeFileValue}</i><span>${previousValue}</span>`;
            }
            else {
                dataas[4] = previousValue;
            }

            table.row(selectedRow).data(dataas);
            $body.find(this).remove();
        }
    });

    function downloadFile(filePath) {
        $.ajax({
            url: `/download?path=${filePath}`,
        }).done(function (response) {
            console.log('response is ', response);
            let blob = b64toBlob(response.data, response.type);
            download(new Blob([blob]), response.name, response.type);
        });
    }

    function reloadTable(path) {
        $loader.show();
        let instance = $.jstree.reference($body.find('#filesTree')),
            id = path.replace(root, root !== '/' ? '#' : '#/'),
            node = instance.get_node(id);
        oldPath = '';
        getFilesAndFolders(path, instance, node);
    }

    function getFilesAndFolders(filePath, instance, node) {
        if (oldPath !== filePath) {
            let url = '/listFilesAndFolders',
                query = { path: filePath, id: node.id };
            $.get(url, query, function (files, status) {
                table.clear().draw();
                $.each(files, function (key, file) {
                    let fileArray = $.map([file], function () {
                        let { text, size, modified, extension, id, permission, icon } = file;
                        return [`<i class='${icon} mr-2'></i><span>${text}</span>`, size, modified, extension, permission, id]
                    });
                    table.row.add(fileArray).draw(false);
                    table.column(5).visible(false);
                });
            }).fail(function () {
                table.clear().draw();
            });
            instance.deselect_all();
            disableCurrentControls();
            if (node.id !== '#') {
                instance.open_node(node);
                instance.select_node(node);
            }
            setTimeout(() => {
                $loader.hide();
            }, 500)
        }
        oldPath = filePath;
    }

    function RenameFiles(cellPosition, datas, index, currentRow) {
        let textValue;
        if (index == 0) {
            let splitValue = datas[0].split('</i>')[1].split(/<\/?span>/)[1];
            datas[0] = datas[0].split('</i>')[0];
            textValue = splitValue;
            previousValue = textValue;
        }
        else {
            textValue = cellPosition.innerText;
            previousValue = textValue;
            datas[index] = '';
        }
        console.log('index value is ', index);
        table.row(currentRow).data(datas);
        let renameInput = $(`<input type='text' style='width: 113px;' id='renameInput' value=${textValue}>`);
        $body.find(cellPosition).append(renameInput);
        $body.find(cellPosition).css('display', 'inline-flex');
        console.info('cellPosition ', cellPosition);
        firstClicked = datas[0];
    }

    function afterDelete() {
        table.row(selectedRow).remove().draw();
        disableCurrentControls();
        toastr.success('Successfully deleted');
    }

    function enableCurrentControls() {
        $.each(currentControls, function (key, control) {
            $body.find(control).removeClass('disabled');
            $body.find(control).addClass('enabled');
        });
        previousControls = currentControls;
    }

    function disableCurrentControls() {
        selectedRow = undefined;
        $.each(currentControls, function (key, control) {
            $body.find(control).removeClass('enabled');
            $body.find(control).addClass('disabled');
        });
        currentControls = previousControls = [];
    }

    function disablePreviousControls() {
        $.each(previousControls, function (key, previousControl) {
            $body.find(previousControl).removeClass('enabled');
            $body.find(previousControl).addClass('disabled');
        });
        previousControls = [];
    }

    function toggleSelectUnselectControl(type) {
        if (type === 'select') {
            $unselectAll.removeClass('disabled');
            $selectAll.addClass('disabled');
        } else {
            $selectAll.removeClass('disabled');
            $unselectAll.addClass('disabled');
            selectedRows = undefined;
        }
        disableCurrentControls();
    }
});




