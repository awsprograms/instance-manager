// entry point
const root = '/';

// jquery function which runs after dom loaded
$(function () {
    // whole body cached for performance
    $body = $('body');
    $.get('/os', function (os, status) {
        console.log('os: ', os);
        let { hd, memory, hostname, type, userInfo, cpus } = os;
        setHardDrive(hd);
        setMemory(memory);
        setHostname(hostname);
        setType(type);
        setUserInfo(userInfo);
        setCPUs(cpus);
    }).fail(function () {

    });
});

function bytesToSize(bytes) {
    let sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes === 0) return '0 Byte';
    let i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return (bytes / Math.pow(1024, i)).toFixed(2) + ' ' + sizes[i];
}

function getPercentage(used, total) {
    return Math.round((used / total) * 100)
}

function getClassAccordingToUsage (percent) {
    if(percent >= 80 && percent < 90) {
        return 'bg-warning';
    } else if (percent >= 90) {
        return 'bg-danger';
    } else {
        return 'bg-success';
    }
}

function setHardDrive({ total: { free, size } }) {
    free = Number(free.replace('GB', ''));
    size = Number(size.replace('GB', ''));
    let used = size - free;
    let percent = getPercentage(used, size);
    $body.find('.hd .detail').text(`${used.toFixed(2)} GB of ${size} GB Used`).end()
    .find('.hd .progress-bar').text(`${percent}% Used`).css({width: `${percent}%`}).addClass(getClassAccordingToUsage(percent))
}

function setMemory({freeMemory, totalMemory}) {
    let used = totalMemory - freeMemory;
    let percent = getPercentage(used, totalMemory);
    $body.find('.memory .detail').text(`${bytesToSize(used)} of ${bytesToSize(totalMemory)} Used`).end()
        .find('.memory .progress-bar').text(`${percent}% Used`).css({width: `${percent}%`}).addClass(getClassAccordingToUsage(percent))
}

function setHostname(hostname) {
    $body.find('.hostname .detail').text(hostname);
}

function setType(type) {
    $body.find('.type .detail').text(type);
}

function setUserInfo(userInfo) {
    $body.find('.userInfo ul').append(`<li><b>Username: </b>${userInfo.username}</li>`);
    $body.find('.userInfo ul').append(`<li><b>Home Dir: </b>${userInfo.homedir}</li>`);
}

function setCPUs(cpus) {
    $.each(cpus, function (key, cpu) {
        $body.find('.cpus ol').append(`<li>${cpu.model}</li>`);
    })
}




