'use strict';

const Hapi = require('hapi');
const Inert = require('inert');
const Path = require('path');
const fs = require('fs');
const Datastore = require('nedb');
const bcrypt = require("bcrypt");
const HapiAuthCookie = require('hapi-auth-cookie');
const projects = require('./server/apis/projects');
const routes = require('./server/routes/index');

let db = {};

db.projects = new Datastore({ filename: 'db/projects.db', autoload: true });
db.users = new Datastore({ filename: 'db/users.db', autoload: true });

let admin = {
  userId: 'admin',
  pass: 'admin'
};

db.users.findOne({userId: admin.userId}, function(err, doc) {
  if(doc) {
    // console.log('User already exist with ID', doc._id);
    return;
  }
  /*
  * Start encrypting password to hashes
  * */
  bcrypt
    .hash(admin.pass, 10)
    .then(hash => {
      // Store hash in DB as password.
      db.users.insert({userId: admin.userId, pass: hash}, function(err, doc) {
        if (err) {
          console.log('err ', err);
          return;
        }
        console.log('Inserted', doc.userId, 'with ID', doc._id);
        console.log(doc);
      });
    })
    .catch(err => console.error(err.message));
});

// let project = [
//   {
//     name: 'Project1',
//     desc: `This is Project1 detail description`,
//     type: 'static',
//     port: '80',
//     path: '/user/project1',
//     main: 'index.html',
//     engine: '',
//     createdAt: new Date(),
//     updatedAt: null
//   },
//   {
//     name: 'Project2',
//     desc: `This is Project2 detail description`,
//     type: 'dynamic',
//     port: '3000',
//     path: '/user/project2',
//     main: 'index.html',
//     engine: '',
//     createdAt: new Date(),
//     updatedAt: null
//   },
//   {
//     name: 'Project3',
//     desc: `This is Project3 detail description`,
//     type: 'static',
//     port: '80',
//     path: '/user/project3',
//     main: 'index.html',
//     engine: '',
//     createdAt: new Date(),
//     updatedAt: null
//   }
// ];

// db.projects.ensureIndex({ fieldName: "_id", unique: true }, function(err) {
//   if(err) console.log("Error creating or loading indexes. " + err);
// });

// project.map((p) => {
//   db.projects.insert(p, function (err, doc) {
//     if (err) {
//       console.log('err ', err);
//       return;
//     }
//     console.log('Inserted', doc.name, 'with ID', doc._id);
//   });
// });

/* create a server with a host and port */
const server = Hapi.server({
  host: 'localhost',
  port: 8444,
  routes: {
    files: {
      relativeTo: Path.join(__dirname, 'public')
    }
  }
});

var walkSync = function (dir, filelist) {
  var fs = fs || require('fs'),
    files = fs.readdirSync(dir);
  filelist = filelist || [];
  files.forEach(function (file) {
    if (fs.statSync(dir + '/' + file).isDirectory()) {
      filelist = walkSync(dir + '/' + file, filelist);
    }
    else {
      filelist.push(dir + '/' + file);
    }
  });
  return filelist;
};

projects(server, db);
routes(server, walkSync, db);

/* start server */
async function start() {
  await server.register(Inert);
  await server.register(HapiAuthCookie);
  
  let users = [];
  
  await db.users.find({}, (err, data) => {
    users = data;
  });
  
  const cache = server.cache({ segment: 'sessions', expiresIn: 3 * 24 * 60 * 60 * 1000 });
  server.app.cache = cache;
  
  server.auth.strategy('session', 'cookie', {
    password: 'abcdefghijklmnopqrstuvwxyz123456',
    cookie: 'session',
    ttl:  24 * 60 * 60 * 1000, // 24 hours from now
    redirectTo: '/login',
    isSecure: false,
    validateFunc: async (request, session) => {
      console.info('session ', session);
      const cached = await cache.get(session.sid);
      const logs = {
        valid: !!cached
      };
      if (logs.valid) {
        logs.credentials = cached.account;
      }
      // console.info('logs ', logs);
      return logs;
    }
  });
  
  server.route({
    method: 'GET',
    path: '/{param*}',
    config: {
      auth: 'session',
      handler: {
        directory: {
          path: '.',
          redirectToSlash: true,
          index: true,
        }
      }
    }
  });
  
  server.route({
    method: 'GET',
    path: '/login',
    options: {
      handler: (request, h) => {
        return h.file('login.html');
      }
    }
  });
  
  try {
    await server.start();
  }
  catch (err) {
    console.log(err);
    process.exit(1);
  }
  
  console.log('Server running at:', server.info.uri);
}

start();
