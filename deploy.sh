#!/usr/bin/env bash
zip ec2-manager.zip package.json server.js
scp -i /Users/muhammadfurqan/Documents/work/certificates/appbakerz-ec2.pem ec2-manager.zip ec2-user@ec2-54-172-59-77.compute-1.amazonaws.com:~
# Login to Container
ssh -i /Users/muhammadfurqan/Documents/work/certificates/appbakerz-ec2.pem ec2-user@ec2-54-172-59-77.compute-1.amazonaws.com <<ENDSSH
    #commands to run on remote host
    cd ~
    rm -r ec2-manager
    mkdir ec2-manager
    unzip ec2-manager.zip -d ec2-manager
    cd ec2-manager
    npm install
    npm start
ENDSSH
# List folders so we know that is working perfectly
