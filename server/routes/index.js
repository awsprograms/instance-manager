'use strict';

const fs = require('fs');
const fse = require('fs-extra');
const util = require('util');
const os = require('os');
const unzip = require('unzip');
const fsStream = require('fstream');
var hddSpace = require('hdd-space');
const exec = util.promisify(require('child_process').exec);
const readdir = util.promisify(fs.readdir);
const lstat = util.promisify(fs.lstat);
const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);
const appendFile = util.promisify(fs.appendFile);
const chmodPromise = util.promisify(fs.chmod);
const rmdir = util.promisify(fs.rmdir);
const unlink = util.promisify(fs.unlink);
const renameFileOrFolder = util.promisify(fs.rename);
const open = util.promisify(fs.open);
const close = util.promisify(fs.close);
const mkdir = util.promisify(fs.mkdir);
const copy = util.promisify(fse.copy);
const move = util.promisify(fse.move);
const fsStat = util.promisify(fs.stat);
const readStream = util.promisify(fs.createReadStream);
const {map, filter} = require('p-iteration');
const mime = require('mime');
const archiver = require('archiver');
const rimraf = util.promisify(require('rimraf'));
const CsvReadableStream = require('csv-reader');
const trash = require('trash');
const emptyTrash = require('empty-trash');
const bcrypt = require("bcrypt");

let uuid = 1;

module.exports = function (server, walkSync, db) {
  server.route({
    method: 'POST',
    path: '/login',
    handler: async (request, h) => {
      let account = null;
      return new Promise((resolve, reject) => {
        const { userId, pass } = request.payload;
        db.users.findOne({userId: userId}, async (err, data) => {
          let res = {
            msg: '',
            status: null,
            authenticated: false
          };
          if(err) {
            reject(err);
            return;
          }
          if(data) {
            await
              bcrypt
              .compare(pass, data.pass)
              .then(isMatch => {
                if(!isMatch) {
                  res.msg = 'Incorrect username or password.';
                  res.status = 401;
                  resolve(res);
                  return
                }
                /*
                * Set response object
                * */
                account = data;
                res.msg = 'Authenticated Successfully.';
                res.info = data;
                res.status = 200;
                res.authenticated = true;
                resolve(res);
              })
              .catch((e) => {
                reject(e);
              });

            const sid = String(++uuid);
            await
              request.server.app.cache.set(sid, { account }, 0);
              request.cookieAuth.set({ sid });
          } else {
            res.msg = `User <b>"${userId}"</b> not found!`;
            res.status = 404;
            resolve(res);
          }
        });
      });
    }
  });
  
  server.route({
    method: 'GET',
    path: '/listFolders',
    handler: async function (request, h) {
      try {
        const path = request.query.id === '#' ? request.query.path : request.query.path + '/' + request.query.id.split('/').pop();
        let list = await readdir(path);
        if (request.query.id === '#') list = await filter(list, async item => item !== 'trash');
        list = await map(list, async (item) => {
          const fullPath = (path !== '/' ? path + '/' : path) + item;
          const stat = await lstat(fullPath);
          if (stat.isDirectory()) // objects returned for folders only
            return {
              text: item,
              id: request.query.id + '/' + item,
              icon: 'fas fa-folder',
              children: true,
              parent: request.query.id
            };
        });
        // for removing undefined values for files from list
        list = await filter(list, async item => item);
        return list || [];
      } catch (err) {
        return h.response({error: err}).code(404);
      }
    }
  });
  
  server.route({
    method: 'POST',
    path: '/saveProject',
    handler: async function (request, h) {
      return new Promise((resolve, reject) => {
        let newProject = request.payload;
        newProject['createdAt'] = new Date();
        newProject['updatedAt'] = null;
        db.projects.insert(newProject, function (err, doc) {
          if (err) {
            console.log('err ', err);
            reject(err);
            return;
          }
          resolve(doc);
        });
      })
    }
  });
  
  server.route({
    method: 'POST',
    path: '/updateProject',
    handler: async function (request, h) {
      return new Promise((resolve, reject) => {
        let updatedProject = request.payload;
        updatedProject['updatedAt'] = new Date();
        updatedProject['_id'] = updatedProject['id'];
        delete updatedProject['id'];
        db.projects.ensureIndex({ fieldName: "_id", unique: true }, function(err) {
          if(err) console.log("Error creating or loading indexes. " + err);
        });
        db.projects.update({_id: updatedProject._id}, { $set: updatedProject }, {}, function (err, replaceDoc) {
          if (err) {
            console.log('err ', err);
            reject(err);
            return;
          }
          console.info(replaceDoc);
          resolve(replaceDoc);
        });
      })
    }
  });
  
  server.route({
    method: 'GET',
    path: '/listProjects',
    handler: async function (request, h) {
      return new Promise((resolve, reject) => {
        db.projects.find({}).sort({name: 1}).exec(function (err, docs) {
          if(err) {
            console.log('err: ', err);
            reject(err);
            return;
          }
          resolve(docs);
        });
      })
    }
  });
  
  server.route({
    method: 'GET',
    path: '/listFilesAndFolders',
    handler: async function (request, h) {
      try {
        const path = request.query.path;
        let list = await readdir(path),
          isRoot = (path.length - path.replace(/\//g, '').length) === 1;
        if (isRoot && !request.query.path.includes('trash')) list = await filter(list, async item => item !== 'trash');
        else if (request.query.path.includes('trash')) list = await filter(list, async item => item !== 'trashFile.csv');
        list = map(list, async (item) => {
          const fullPath = path + '/' + item;
          const stat = await lstat(fullPath);
          // identifying file mime type
          if (!stat.isDirectory()) {
            var mimeType = mime.getType(item);
            if (!mimeType) mimeType = 'unknown';
          }
          // calculating file/folder size
          const i = stat.size !== 0 ? Math.floor(Math.log(stat.size) / Math.log(1024)) : 0,
            size = (stat.size / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + ['bytes', 'KB', 'MB', 'GB', 'TB'][i];
          // calculating last modified time
          const lastModified = new Date(stat.mtime).toUTCString().split(' ').slice(1, 5).join(' ');
          let modeTypes = [];
          modeTypes.push((stat["mode"] & 400 ? 'r' : '-') + '' + (stat["mode"] & 200 ? 'w' : '-') + '' + (stat["mode"] & 100 ? 'x' : '-'));
          modeTypes.push((stat["mode"] & 4 ? 'r' : '-') + '' + (stat["mode"] & 2 ? 'w' : '-') + '' + (stat["mode"] & 1 ? 'x' : '-'));
          modeTypes.push((stat["mode"] & 40 ? 'r' : '-') + '' + (stat["mode"] & 20 ? 'w' : '-') + '' + (stat["mode"] & 10 ? 'x' : '-'));
          
          let octalCode = '0';
          //console.log('modeTypes ', modeTypes);
          for (let i = 0; i < modeTypes.length; i++) {
            if (modeTypes[i] == 'rwx') {
              octalCode = octalCode + '7';
            }
            else if (modeTypes[i] == 'r-x') {
              octalCode = octalCode + '5';
            }
            else if (modeTypes[i] == '-w-') {
              octalCode = octalCode + '2';
            }
            else if (modeTypes[i] == '--x') {
              octalCode = octalCode + '1';
            }
            else if (modeTypes[i] == '-wx') {
              octalCode = octalCode + '3';
            }
            else if (modeTypes[i] == 'r--') {
              octalCode = octalCode + '4';
            }
            else if (modeTypes[i] == 'rw-') {
              octalCode = octalCode + '6';
            }
            else if (modeTypes[i] == '---') {
              octalCode = octalCode + '0';
            }
          }
          //console.log(otherMode+''+groupMode+''+ownerMode);
          const permissions = octalCode;
          return {
            text: item,
            id: request.query.id + '/' + item,
            icon: stat.isDirectory() ? 'fas fa-folder' : 'fas fa-file',
            children: stat.isDirectory(),
            size,
            parent: request.query.id,
            modified: lastModified,
            extension: stat.isDirectory() ? 'directory' : mimeType,
            permission: permissions
          };
        });
        return list || [];
      } catch (err) {
        return h.response({error: err}).code(404);
      }
    }
  });
  
  server.route({
    method: 'DELETE',
    path: '/file',
    handler: async function (request, h) {
      try {
        await unlink(request.query.path);
        return h.response().code(200);
      } catch (err) {
        return h.response({error: err}).code(404);
      }
    }
  });
  
  server.route({
    method: 'DELETE',
    path: '/folder',
    handler: async function (request, h) {
      try {
        await rmdir(request.query.path);
        return h.response().code(200);
      } catch (err) {
        return h.response({error: err}).code(403);
      }
    }
  });
  
  server.route({
    method: 'DELETE',
    path: '/forceFolder',
    handler: async function (request, h) {
      try {
        await rimraf(request.query.path);
        return h.response().code(200);
      } catch (err) {
        return h.response({error: err}).code(403);
      }
    }
  });
  
  server.route({
    method: 'POST',
    path: '/copy',
    handler: async function (request, h) {
      try {
        let src = request.payload.src + '/' + request.payload.name,
          dest = request.payload.dest + '/' + request.payload.name;
        await copy(src, dest);
        return h.response().code(200);
      } catch (err) {
        return h.response({error: err}).code(403);
      }
    }
  });
  
  server.route({
    method: 'POST',
    path: '/move',
    handler: async function (request, h) {
      try {
        let src = request.payload.src + '/' + request.payload.name,
          dest = request.payload.dest + '/' + request.payload.name;
        await move(src, dest, {overwrite: true});
        return h.response().code(200);
      } catch (err) {
        return h.response({error: err}).code(403);
      }
    }
  });
  
  server.route({
    method: 'POST',
    path: '/file',
    handler: async function (request, h) {
      try {
        let file = request.query.path + '/' + request.query.name;
        let newFile = await open(file, 'wx');
        await close(newFile);
        return h.response().code(200);
      } catch (err) {
        return h.response({error: err}).code(403);
      }
    }
  });
  
  server.route({
    method: 'POST',
    path: '/folder',
    handler: async function (request, h) {
      try {
        let file = request.query.path + '/' + request.query.name;
        await mkdir(file);
        return h.response().code(200);
      } catch (err) {
        return h.response({error: err}).code(403);
      }
    }
  });
  
  server.route({
    method: 'POST',
    path: '/upload',
    options: {
      payload: {
        output: 'stream',
        parse: true,
        allow: 'multipart/form-data',
        // raise file size upload limit from 1MB to 256MB
        maxBytes: 268435456
      },
      handler: async function (request, h) {
        try {
          let count = 0, exist, overwrite = (request.payload.overwrite === "true"); // converting string to boolean
          for (let key in request.payload) {
            if (key.match(/file/i))
              count = count + 1;
          }
          // for checking existing file
          if (!overwrite)
            for (let i = 0; i < count; i++) {
              let file = request.payload[`file[${i}]`],
                fileName = file.hapi.filename,
                path = request.payload.path,
                fullPath = path + '/' + fileName;
              try {
                let existingFile = await open(fullPath, 'r');
                exist = true;
                await close(existingFile);
                break;
              }
              catch (error) {
              }
            }
          // uploading file when not exists
          if (!exist) {
            for (let i = 0; i < count; i++) {
              let file = request.payload[`file[${i}]`],
                fileName = file.hapi.filename,
                path = request.payload.path,
                data = file._data,
                fullPath = path + '/' + fileName;
              await writeFile(fullPath, data);
            }
            return h.response().code(200);
          } else throw 'File already exists';
        }
        catch (err) {
          return h.response({error: err}).code(403);
        }
      }
    }
  });
  
  server.route({
    method: 'GET',
    path: '/file',
    handler: async function (request, h) {
      try {
        const content = await readFile(request.query.path);
        return content;
      } catch (err) {
        return {error: err};
      }
    }
  });
  
  server.route({
    method: 'POST',
    path: '/updateFile',
    handler: async function (request, h) {
      try {
        await writeFile(request.query.path, request.payload.data);
        return true;
      } catch (err) {
        return {error: err};
      }
    }
  });
  
  server.route({
    method: 'GET',
    path: '/download',
    handler: async function (request, h) {
      try {
        const filePath = request.query.path;
        const fileName = filePath.split('/').pop();
        const data = await readFile(filePath, 'base64');
        const mimeType = mime.getType(fileName);
        return h.response({data: data, type: mimeType, name: fileName}).code(200);
      } catch (err) {
        return h.response({error: err}).code(404);
      }
    }
  });
  
  server.route({
    method: 'POST',
    path: '/renameFile',
    handler: async function (request, h) {
      try {
        const splittedValue = request.query.path.split('/').pop();
        const queryPathValue = request.query.path.split(splittedValue);
        let newNameFile = queryPathValue[0] + request.query.name;
        let isContain = false;
        try {
          const stats = await fsStat(queryPathValue[0] + request.query.name);
          console.log('checking!!!');
          isContain = true;
          return {message: 'exist'}
          
        } catch (error) {
        }
        if (!isContain) {
          console.log('new file name is ', newNameFile);
          await renameFileOrFolder(request.query.path, newNameFile);
          console.log('file is not contains!!!');
        }
        return {message: 'success', newFile: newNameFile}
      } catch (err) {
        return {error: err}
      }
    }
  });
  
  server.route({
    method: 'POST',
    path: '/CreateZipFile',
    handler: async function (request, h) {
      try {
        let zipFilePath = request.payload.newPath,
          output = fs.createWriteStream(zipFilePath),
          archive = archiver(request.payload.extName, {
            zlib: {level: 9}
          });
        let fullValue = [];
        
        output.on('close', function () {
        });
        
        output.on('end', function () {
        });
        
        // good practice to catch warnings (ie stat failures and other non-blocking errors)
        archive.on('warning', function (err) {
          if (err.code === 'ENOENT') {
            // log warning
          } else {
            // throw errore
            throw err;
          }
        });
        
        archive.on('error', function (err) {
          throw err;
        });
        
        // pipe archive data to the file
        archive.pipe(output);
        for (let i = 0; i < request.payload.totalFile; i++) {
          let valuePath1 = request.payload.path + '/' + request.payload['selectedFile' + i],
            fileOrFolderstat = await lstat(valuePath1);
          
          if (fileOrFolderstat.isDirectory()) {
            archive.directory(valuePath1 + '/', request.payload['selectedFile' + i]);
            let FullArray = [];
            let subDirPath = valuePath1;
            fullValue.push(walkSync(subDirPath, FullArray));
            console.log('FullArray value is ', FullArray);
          }
          else if (fileOrFolderstat.isFile()) {
            const readFile = fs.createReadStream(valuePath1);
            archive.append(readFile, {name: request.payload['selectedFile' + i]});
            fullValue.push(request.payload.path + '/' + request.payload['selectedFile' + i]);
          }
        }
        archive.finalize();
        return {message: 'success', fullValue}
      } catch (err) {
        return {error: err}
      }
    }
  });
  
  server.route({
    method: 'POST',
    path: '/unzipFileOrFolder',
    handler: function (request, h) {
      try {
        // let test = new Promise((resolve, reject) => {
        let fullPathValue = request.payload.path + '/' + request.payload.selectedFile,
          fullValue = [],
          FullArray = [],
          responseValue;
        let readStream = fs.createReadStream(fullPathValue);
        let writeStream = fsStream.Writer(request.payload.newpathValue + '/');
        readStream
          .pipe(unzip.Parse())
          .pipe(writeStream);
        
        // return readStream.on('end', async () => {
        //     let folderOrFile = request.payload.selectedFile.split('.')[0];
        //     const stat = await lstat(request.payload.newpathValue + '/' + folderOrFile);
        //     if (stat.isDirectory()) {
        //         fullValue.push(walkSync(request.payload.newpathValue + '/', FullArray));
        //         console.log(fullValue);
        //         return { message: 'success', fullValue }
        //     }
        //     else {
        //         let splitValue = fullPathValue.split('.')[0];
        
        fullValue.push(splitValue);
        console.log(fullValue);
        return {message: 'success', fullValue};
      }
      catch (error) {
        console.log(error);
        return {error}
      }
    }
  });
  
  server.route({
    method: 'DELETE',
    path: '/trash',
    handler: async function (request, h) {
      try {
        // os.
        await trash([request.query.path]);
        return ({message: 'success'});
      } catch (err) {
        console.log(err);
        return h.response({error: err}).code(404);
      }
    }
  });
  
  server.route({
    method: 'POST',
    path: '/restoreFileOrFolder',
    handler: async function (request, h) {
      try {
        const csvPathFile = request.payload.path + '/trashFile.csv';
        let dataArray = [];
        let newStream = [];
        let inputStream = fs.createReadStream(csvPathFile, 'utf8');
        console.log('name is ', request.payload.name);
        inputStream
          .pipe(CsvReadableStream({parseNumbers: true, parseBooleans: true, trim: true}))
          .on('data', function (row) {
            dataArray.push(row);
          })
          .on('end', function (data) {
            //console.log('No more rows!');
            let dest;
            dataArray.forEach(function (value) {
              console.log('value is', value[0].trim());
              if (value[0].trim() == request.payload.name) {
                //console.log('found value in csv is ', value[1]
                console.log('found it');
                dest = value[1].trim();
              }
              else {
                newStream.push(value);
              }
            });
            if (dest) {
              let filename = request.payload.path + '/' + request.payload.name;
              move(filename, dest);
              writeFile(csvPathFile, newStream);
            }
          });
        
        return {message: 'sucess'}
      }
      catch (err) {
        return {error: err}
      }
    }
  });
  
  server.route({
    method: 'POST',
    path: '/changePermission',
    handler: async function (request, h) {
      try {
        console.log('payload value ', request.payload);
        await chmodPromise(request.payload.path, request.payload.mode);
        return {message: 'success'}
      }
      catch (err) {
        return {error: err};
      }
    }
  });
  
  server.route({
    method: 'DELETE',
    path: '/emptyTrashBin',
    handler: async function (request, h) {
      try {
        await emptyTrash();
        return {message: 'success'}
      }
      catch (err) {
        return {error: err}
      }
    }
  });
  
  server.route({
    method: 'GET',
    path: '/nginx/{cammond}',
    handler: async function (request, h) {
      try {
        const {stdout, stderr} = await exec(request.params.cammond);
        console.log(stdout);
        if (stderr) {
          return {message: stderr};
        } else {
          return {message: stdout || 'Successfully executed!'};
        }
      }
      catch (err) {
        return {error: err};
      }
    }
  });
  
  server.route({
    method: 'GET',
    path: '/os',
    handler: async function (request, h) {
      const hd = await hddSpace.fetchHddInfo({format: 'auto'})
      return {
        memory: {freeMemory: os.freemem(), totalMemory: os.totalmem()},
        homedir: os.homedir(),
        type: os.type(),
        userInfo: os.userInfo(),
        hostname: os.hostname(),
        networkInterfaces: os.networkInterfaces(),
        platform: os.platform(),
        uptime: os.uptime(),
        hd: hd,
        cpus: os.cpus(),
      }
    }
  });
};