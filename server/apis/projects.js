module.exports = function(server, db) {
    db.projects.loadDatabase();

    server.method('dbFind', (collection, query) => {
        return new Promise((resolve, reject) => {
            db[collection].find(query || {}, function (err, projects) {
                if(projects) {
                    resolve(projects)
                } else {
                    reject(err)
                }
            });
        });
    });

    server.route({
        method: 'GET',
        path: '/projects',
        handler: async function (request) {
            try {
                return await server.methods.dbFind('projects', {});
            } catch (err) {
                return { error: err };
            }
        }
    });
};
